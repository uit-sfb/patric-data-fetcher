FROM python:3

LABEL maintainer="mmp@uit.no" \
  toolVersion=0.1 \
  toolName=patric-data-fetcher \
  url=https://gitlab.com/uit-sfb/patric-data-fetcher

WORKDIR /app/patric

ENV HOME /app/patric

RUN chmod g+rwx $HOME

COPY src/requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY /src/*.py ./

ENTRYPOINT [ "python", "./getMarineRecords.py" ] 
