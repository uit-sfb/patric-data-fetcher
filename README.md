The process of running the script could be done via gitlab.

 - User may create a new branch from master with the specific name, for example: run_from_2022-01-01, where 2022-01-01 refers to starting date in the format yyyy-mm-dd.

 - Based on the date specified in the branch name, the script will retrieve the data from the PATRIC database and save the output as an artifact 

 At the moment, most of the filtering is done by the script which means it will browse through all available data first and filter the resutls, which can usually take up to few hours.
