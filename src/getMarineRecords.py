import argparse
import datetime
import json
import sys
from multiprocessing import Pool, cpu_count

import pandas as pd
import requests

NOW = datetime.datetime.now()

def main(argv):
    parser = argparse.ArgumentParser(description='Set email and other parameters')
    parser.add_argument('-o',dest='out',default='',help='output directory')
    parser.add_argument('-i',dest='input',default='',help='list of host names to filter')
    parser.add_argument('--start_date',dest='start',default=str(NOW.date()),help='starting collection year')
    parser.add_argument('--end_date',dest='end',default=str(NOW.date()),help='end collection year')
    args = parser.parse_args()
    return  args.input, args.out, args.start, args.end;

def set_front_column(col,df):
    cols = list(df)
    cols.insert(0,cols.pop(cols.index(col)))
    df = df.loc[:, cols]
    return df

def convert_to_dataframe(tsv):
    df = pd.read_csv(tsv,sep='\t',engine='python',index_col=False)
    df.columns = df.columns.str.strip()
    return df

def write_to_tsv(df,outPath):
    if not df.empty:
        df.to_csv(outPath,sep='\t',index=False)

def convert_to_dateobj(datestr):
    return datetime.datetime.strptime(datestr, "%Y-%m-%d")

def get_date_inserted(full_date):
    if full_date:
        date_inserted = full_date.split('T')[0]
        date_obj = convert_to_dateobj(date_inserted)
        return date_obj
    return ''

"""Comment out end_date, since we aren't using it at the moment"""
def compare_dates(start_date, end_date, date_inserted):
    if date_inserted:
        return convert_to_dateobj(start_date) <= date_inserted #<= convert_to_dateobj(end_date)
    return False

def make_call(count):
    core = "https://www.patricbrc.org/api"
    type = "genome"
    q_filter = "ne(kingdom,Viruses)"
    query = "?{0}&limit(25000,{1})&http_accept=application/json".format(q_filter,count)
    url = "/".join([core,type,query])
    print(url)
    try:
        r = requests.get(url)
        data = json.loads(r.text)
        return data
    except Exception as e:
        raise e

def flatten(t):
    return [item for sublist in t for item in sublist]


def get_hosts(path):
    df = convert_to_dataframe(path)
    hosts = list(df.iloc[:,0].values)
    return hosts

def pull_data(start_date,end_date,input):
    count_list = [i for i in range(0,5000000,25000)]
    batches = list(map(make_call,count_list))
    list_of_data = flatten(batches)
    hosts = get_hosts(input)
    filtered_by_date = filter(lambda x: compare_dates(start_date,end_date,get_date_inserted(x.get('date_inserted',''))),list_of_data)
    filtered_by_host = filter(lambda x: x.get('host_name','').lower() not in hosts, filtered_by_date)
    return filtered_by_host

def get_df(obj):
    try:
        df = pd.json_normalize(obj)
        return df
    except Exception as e:
        raise e

def pipe_values(df):
    columns = list(df)
    for col in columns:
        df[col] = df[col].apply(lambda x: '|'.join(x) if isinstance(x,list) else x)
    return df

if __name__ == "__main__":
    input, output, start_date, end_date = main(sys.argv[1:])
    entries = list(pull_data(start_date,end_date,input))
    print("Entries found: {}".format(len(entries)))
    df = pd.DataFrame()
    pool = Pool(cpu_count()-1)

    df_list = pool.map(get_df,entries)
    pool.close()
    pool.join()

    df_list.append(df)

    res = pd.concat(df_list)
    res = set_front_column("genome_id",res)
    res = pipe_values(res)

    write_to_tsv(res,output)
